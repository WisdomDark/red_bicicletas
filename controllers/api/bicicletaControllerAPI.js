var Bicicleta = require('../../models/bicicleta');

exports.bicicletaList = function(req,res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    })
}

exports.bicicletaCreate = function(req,res){
    var bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo);
    bici.ubicacion = [req.body.lat,req.body.lng];
    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });
}
exports.bicicletaDelete = function(req,res){
    var biciId = req.body.id;
    Bicicleta.removeById(biciId);
    res.status(204).send();
}

exports.bicicletaUpdate = function(req,res){
    
    var bici = Bicicleta.findById(req.body.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat,req.body.lng];

    res.status(200).json({
        bicicleta: bici
    });
}
