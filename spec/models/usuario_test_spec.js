var mongoose = require('mongoose');
const bicicleta = require('../../models/bicicleta');
const Bicicleta = require('../../models/bicicleta');
var Reserva = require('../../models/reserva');
const Usuario = require('../../models/usuario');

describe('Testing Usuarios', () => {

    beforeEach((done) => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        var mongoDB = 'mongodb://localhost/testdb'
        mongoose.connect(mongoDB)

        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'connection error'))
        db.once('open', () => {
            console.log('We are connected to test database')
        });
        done();
    });

    afterEach((done) => {
        Reserva.deleteMany({},(err,result) => {
            if(err) console.log(err)
            Usuario.deleteMany({},function(err, success){
                if(err) console.log(err)
                Bicicleta.deleteMany({},function(err, success){
                    if(err) console.log(err)
                })
            })
            done();
        });
    });

    describe('Cuando un usuario reserva una bici',() => {
        it('Debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre:'Exzequiel'})
            usuario.save()
            const bicicleta = new Bicicleta({code:1, color:"verde",modelo:"urbana",ubicacion:[-34.5, -54.1]})
            bicicleta.save()

            var hoy = new Date();
            var mañana = new Date();
            mañana.setDate(hoy.getDate() + 1)
            usuario.reservar(bicicleta.id,hoy,mañana,function(err,reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err,reservas){
                    console.log(reservas[0])
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                })
            })
        })
    })
    
})