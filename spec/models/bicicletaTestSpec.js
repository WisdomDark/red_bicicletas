var mongoose = require('mongoose')
var Bicicleta = require('../../models/bicicleta')

describe('Testing bicicleta', () => {

    beforeEach((done) => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        var mongoDB = 'mongodb://localhost/red_bicicletas'
        mongoose.connect(mongoDB)

        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'connection error'))
        db.once('open', () => {
            console.log('We are connected to test database')
        });
        done();
    });

    afterEach((done) => {
        Bicicleta.deleteMany({},(err,result) => {
            if(err) console.log(err)
            done();
        });
    });

    

    describe('Bicicleta.createInstance',() => {
        it('Crea una instancia de bicicleta', (done) => {
            var bici = Bicicleta.createInstance(1, "verde","urbana",[-34.5, -54.1])
            console.log(bici)
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5)
            expect(bici.ubicacion[1]).toEqual(-54.1)
            done();
        })
    })

    describe('Bicicleta.allBicis',() => {
        it('Comienza vacia', (done) => {
            Bicicleta.allBicis(function(err,bicis){
                if(err) console.log(err)
                console.log('Bicicletas',bicis.length);
                expect(bicis.length).toBe(0);
                done();
            });
        });
        
    });
    

    describe('Bicicleta.add',() => {
        it('Agrega una bicicleta', function(done){
            var bici = new Bicicleta({code:1,color:"verde",modelo:"urbano"})
            Bicicleta.add(bici,function(err,newBici) {
                if(err) console.log(err)
                Bicicleta.allBicis((err,bicis) => {
                    console.log(bicis);
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(bici.code);
                    done();
                })
            })
        })
    })

    describe('Bicicleta.findByCode',() => {
        it('Debe devolver la bicicleta con el codigo 1', function(done){
            Bicicleta.allBicis((err,bicis) => {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code:1,color:"verde",modelo:"urbano"})
                Bicicleta.add(aBici,function(err,newBici) {
                    if(err) console.log(err)

                    var aBici2 = new Bicicleta({code:1,color:"verde",modelo:"urbano"})
                    Bicicleta.add(aBici2,function(err,newBici) { 
                        if(err) console.log(err)

                        Bicicleta.findByCode(1,(err,targetBici) => {
                            
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        })
                    })
                })
            })

        })
    })
})

// var Bicicleta = require('../../models/bicicleta');

// beforeEach(() => {
//     Bicicleta.allBicis = [];
// })

// describe('Bicicleta.allBicis', () => {
//     it('comienza vacia', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add', () => {
//     it('agregamos una', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var a = new Bicicleta(1,'rojo','urbana',[4.6586601, -74.1592163]);
//         Bicicleta.add(a);
//         expect(Bicicleta.allBicis[0]).toBe(a);
//     })
// })

// describe('Bicicleta.findById', () => {
//     it('debe devolver la bici con id 1', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//         var aBici = new Bicicleta(1,'rojo','urbana',[4.6586601, -74.1592163]);
//         var aBici2 = new Bicicleta(2,'verde','urbana',[4.6586601, -74.1592163]);
//         Bicicleta.add(aBici);
//         Bicicleta.add(aBici2);

//         var target = Bicicleta.findById(1)
//         expect(target.id).toBe(1);
//         expect(target.color).toBe(aBici.color);
//         expect(target.modelo).toBe(aBici.modelo);
//     })
// })