var Bicicleta = require('../models/bicicleta');

exports.bicicletaList = function(req,res){
    Bicicleta.allBicis().exec((err, bicis) => {
        res.render('bicicletas/index', {bicis});
    })
}

exports.bicicletaCreateGet = function(req,res){
    res.render('bicicletas/create',{title: 'Crear bicicleta'});
}

exports.bicicletaCreatePost = function(req,res){
    var bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo);
    bici.ubicacion = [req.body.lat,req.body.lng];
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

exports.bicicletaDeletePost = function(req,res){
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}

exports.bicicletaUpdateGet = function(req,res){

    Bicicleta.findByCode(req.params.id).exec((err, bici) => {
        console.log(bici)
        res.render('bicicletas/update', {
          bici
        });
    })
}

exports.bicicletaUpdatePost = function(req,res){
    var bici = Bicicleta.findByCode(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat,req.body.lng];
    
    res.redirect('/bicicletas');
}