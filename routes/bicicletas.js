var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicleta')

router.get('/', bicicletaController.bicicletaList);
router.get('/create', bicicletaController.bicicletaCreateGet);
router.post('/create', bicicletaController.bicicletaCreatePost);
router.post('/:id/delete', bicicletaController.bicicletaDeletePost);
router.get('/:id/update', bicicletaController.bicicletaUpdateGet);
router.post('/:id/update', bicicletaController.bicicletaUpdatePost);

module.exports = router;
