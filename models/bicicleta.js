var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color:String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: {
            type: '2dsphere',
            sparse: true
        }
    }
})

bicicletaSchema.statics.createInstance = function(code,color,modelo,ubicacion){
    return new this({
        code:code,
        color:color,
        modelo:modelo,
        ubicacion:ubicacion
    })
}

bicicletaSchema.methods.toString = () => {
    return `code: ${this.code} | color: ${this.color}`
}

bicicletaSchema.statics.allBicis = function(cb) {
    const result = this.find({},cb);
    console.log(result)
    return result;
}

bicicletaSchema.statics.add = function(bici,cb) {
    return this.create(bici,cb);
}

bicicletaSchema.statics.findByCode = function(aCode,cb) {
    let result =  this.findOne({code:aCode},cb);
    console.log(result);
    return result
}

bicicletaSchema.statics.removeByCode = function(aCode,cb) {
    return this.deleteOne({code:aCode},cb);
}

module.exports = mongoose.model('Bicicleta',bicicletaSchema)