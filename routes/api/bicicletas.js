var express = require('express');
var router = express.Router();
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI')

router.get('/', bicicletaController.bicicletaList);
router.post('/create', bicicletaController.bicicletaCreate);
router.post('/delete', bicicletaController.bicicletaDelete);
router.post('/update', bicicletaController.bicicletaUpdate);

module.exports = router;
