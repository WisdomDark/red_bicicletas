var mongoose = require('mongoose')
var Bicicleta = require('../../models/bicicleta')
var request = require('request')

describe('Bicicleta API', () => {

    beforeEach((done) => {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        var mongoDB = 'mongodb://localhost/red_bicicletas'
        mongoose.connect(mongoDB)

        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'connection error'))
        db.once('open', () => {
            console.log('We are connected to test database')
        });
        done();
    });

    afterEach((done) => {
        Bicicleta.deleteMany({},(err,result) => {
            if(err) console.log(err)
            done();
        });
    });

    describe('GET Bicicletas /',() => {
        it('Status 200', (done) => {
            request.get('http://localhost:3000/api/bicicletas',(err,response,body) => {
            console.log(body)    
                var result = JSON.parse(body)
                console.log(result);
                expect(response.statusCode).toBe(200);
                done();
            })
        })
    })

})
